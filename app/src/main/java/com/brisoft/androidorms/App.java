package com.brisoft.androidorms;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.brisoft.androidorms.persistance.dblibraries.greendao.DatabaseProvider;
import com.brisoft.androidorms.persistance.dblibraries.ormlite.ORMLiteDatabaseHelper;
import com.brisoft.androidorms.persistance.dblibraries.room.AppDatabase;
import com.brisoft.androidorms.persistance.dblibraries.sprinkles.CarTable;
import com.orm.SugarContext;
import com.raizlabs.android.dbflow.config.FlowManager;

import se.emilsjolander.sprinkles.Migration;
import se.emilsjolander.sprinkles.Sprinkles;

/**
 * Created by carlos on 25/07/17.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initSugar();
        initSprinkles();
        initRoom();
        initORMLite();
        initGreenDao();
        initDbFlow();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        closeSugar();
        closeGreenDao();
    }

    private void initSugar(){
        SugarContext.init(getApplicationContext());
    }

    private void closeSugar(){
        SugarContext.terminate();
    }

    private void initSprinkles(){
        Sprinkles sprinkles = Sprinkles.init(getApplicationContext());

        sprinkles.addMigration(new Migration() {
            @Override
            protected void doMigration(SQLiteDatabase db) {
                db.execSQL(
                        "CREATE TABLE "             + CarTable.TABLE_NAME +
                        "("+
                        CarTable.COLUMN_ID          + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        CarTable.COLUMN_MODEL       + " TEXT, " +
                        CarTable.COLUMN_YEAR        + " INTEGER, "+
                        CarTable.COLUMN_HP          + " INTEGER, " +
                        CarTable.COLUMN_HEX_COLOR   + " STRING" +
                        ")"
                );
            }
        });
    }

    private void initRoom(){
        AppDatabase.initialize(getApplicationContext());
    }

    private void initORMLite(){
        ORMLiteDatabaseHelper.initialize(getApplicationContext());
    }

    private void initGreenDao(){
        DatabaseProvider.initialize(getApplicationContext());
    }

    private void closeGreenDao(){
        DatabaseProvider.close();
    }

    private void initDbFlow(){
        FlowManager.init(getApplicationContext());
    }
}
