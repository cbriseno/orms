package com.brisoft.androidorms.concurrent;

import android.util.Log;

import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.persistance.repository.CarRepository;

import java.util.List;

/**
 * Created by carlos on 24/07/17.
 */
public class updateAllRecordsAsyncTask extends BaseAsyncTask {

    private static final String TAG = "updateAllRecordsAsyncTask";

    public updateAllRecordsAsyncTask(CarRepository carRepository) {
        super(carRepository);

        setTaskDescription("Update all records");
    }

    @Override
    protected Void doInBackground(Void... voids) {
        List<Car> carList = getCarRepository().readAll();

        for (Car car: carList){
            car.setModel("Corvette");
        }



        return null;
    }
}
