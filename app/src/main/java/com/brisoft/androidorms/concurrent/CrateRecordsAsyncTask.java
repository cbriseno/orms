package com.brisoft.androidorms.concurrent;

import android.util.Log;

import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.persistance.repository.CarRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlos on 24/07/17.
 */
public class CrateRecordsAsyncTask extends BaseAsyncTask {

    private static final String TAG = "CrateRecordsAsyncTask";
    private static final int RECORDS_TO_CREATE = 500;
    private int recordsToCreate;
    private boolean useFastMethod;

    public CrateRecordsAsyncTask(CarRepository carRepository) {
        this(carRepository, RECORDS_TO_CREATE, false);
    }

    public CrateRecordsAsyncTask(CarRepository carRepository, int recordsToCreate, boolean useFastMethod) {
        super(carRepository);

        this.recordsToCreate = recordsToCreate;
        this.useFastMethod = useFastMethod;
        setTaskDescription("Create and save records");
    }

    @Override
    protected Void doInBackground(Void... voids) {
        List<Car> carList = new ArrayList<>();

        for (int i = 0; i < recordsToCreate; i++) {
            Car car = new Car();

            car.setYear(2017);
            car.setHp(45);
            car.setHexColor("#FF0000");
            car.setModel("Spark");

            carList.add(car);
        }

        if (useFastMethod){
            getCarRepository().createAll(carList);
        }else{
            for (Car car: carList){
                getCarRepository().create(car);
            }
        }

        return null;
    }
}
