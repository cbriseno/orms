package com.brisoft.androidorms.concurrent;

import android.os.AsyncTask;
import android.util.Log;

import com.brisoft.androidorms.persistance.repository.CarRepository;

/**
 * Created by carlos on 24/07/17.
 */
public abstract class BaseAsyncTask extends AsyncTask<Void, Void, Void> {

    private static final String TAG = "BaseAsyncTask";

    private long startTime;
    private CarRepository carRepository;
    private String taskDescription = "default";
    private OnTaskCompletedListener onTaskCompletedListener;

    public BaseAsyncTask(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Log.i(TAG, "Started: "+taskDescription);
        startTime = System.currentTimeMillis();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        Log.i(TAG, "Finished: "+taskDescription);

        if (onTaskCompletedListener != null) {
            onTaskCompletedListener.onTaskCompleted(System.currentTimeMillis() - startTime);
        }
    }

    protected CarRepository getCarRepository() {
        return carRepository;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public BaseAsyncTask setOnTaskCompletedListener(OnTaskCompletedListener onTaskCompletedListener) {
        this.onTaskCompletedListener = onTaskCompletedListener;
        return this;
    }

    public interface OnTaskCompletedListener{
        void onTaskCompleted(long millisRequired);
    }
}
