package com.brisoft.androidorms.concurrent;

import android.util.Log;

import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.persistance.repository.CarRepository;

import java.util.List;

/**
 * Created by carlos on 24/07/17.
 */
public class ReadAllRecordsAsyncTask extends BaseAsyncTask {

    private static final String TAG = "ReadAllRecordsAsyncTask";

    public ReadAllRecordsAsyncTask(CarRepository carRepository) {
        super(carRepository);

        setTaskDescription("Read all records");
    }

    @Override
    protected Void doInBackground(Void... voids) {
        List<Car> carList = getCarRepository().readAll();

        Log.i(TAG, "Records read: "+carList.size());

        return null;
    }
}
