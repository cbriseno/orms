package com.brisoft.androidorms.concurrent;

import android.util.Log;

import com.brisoft.androidorms.persistance.repository.CarRepository;

/**
 * Created by carlos on 24/07/17.
 */
public class DeleteAllAsyncTask extends BaseAsyncTask {

    private static final String TAG = "DeleteAllAsyncTask";

    public DeleteAllAsyncTask(CarRepository carRepository) {
        super(carRepository);

        setTaskDescription("Delete all records");
    }

    @Override
    protected Void doInBackground(Void... voids) {
        Log.i(TAG, "Records deleted: "+ getCarRepository().deleteAll());

        return null;
    }
}
