package com.brisoft.androidorms.concurrent;

import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.persistance.repository.CarRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlos on 24/07/17.
 */
public class CrateRecordsBulkAsyncTask extends BaseAsyncTask {

    private static final String TAG = "CrateRecordsBulkAsyncTask";
    private static final int RECORDS_TO_CREATE = 10000;

    public CrateRecordsBulkAsyncTask(CarRepository carRepository) {
        super(carRepository);

        setTaskDescription("Create and save records in bulk");
    }

    @Override
    protected Void doInBackground(Void... voids) {
        List<Car> carList = new ArrayList<>();

        for (int i = 0; i < RECORDS_TO_CREATE; i++) {
            Car car = new Car();

            car.setYear(2017);
            car.setHp(45);
            car.setHexColor("#FF0000");
            car.setModel("Spark");

            carList.add(car);
        }



        return null;
    }
}
