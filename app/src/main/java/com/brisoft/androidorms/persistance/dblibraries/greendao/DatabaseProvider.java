package com.brisoft.androidorms.persistance.dblibraries.greendao;

import android.content.Context;

import com.brisoft.androidorms.models.DaoMaster;
import com.brisoft.androidorms.models.DaoSession;

import org.greenrobot.greendao.database.Database;

/**
 * Created by carlos on 25/07/17.
 */

public class DatabaseProvider {

    private static DaoSession daoSession;
    private static DaoMaster.DevOpenHelper devOpenHelper;

    public static void initialize(Context context){
        devOpenHelper = new DaoMaster.DevOpenHelper(context, "greendao-db");
        Database database = devOpenHelper.getWritableDb();

        daoSession = new DaoMaster(database).newSession();
    }

    public static DaoSession getDaoSession(){
        return daoSession;
    }

    public static void close(){
        if (devOpenHelper != null){
            devOpenHelper.close();
            devOpenHelper = null;
            daoSession = null;
        }
    }
}
