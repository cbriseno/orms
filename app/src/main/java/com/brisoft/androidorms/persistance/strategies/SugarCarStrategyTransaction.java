package com.brisoft.androidorms.persistance.strategies;

import com.brisoft.androidorms.models.Car;
import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by carlos on 24/07/17.
 */

public class SugarCarStrategyTransaction implements StrategyTransaction<Car> {

    @Override
    public long create(Car record) {
        return SugarRecord.save(record);
    }

    @Override
    public void createAll(List<Car> records) {
        SugarRecord.saveInTx(records);
    }

    @Override
    public Car read(long id) {
        return null;
    }

    @Override
    public List<Car> readAll() {
        return SugarRecord.listAll(Car.class);
    }

    @Override
    public boolean update(Car record) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public boolean deleteAll() {
        long count = SugarRecord.count(Car.class);

        return count  == SugarRecord.deleteAll(Car.class);
    }
}
