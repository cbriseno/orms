package com.brisoft.androidorms.persistance.factory;

import android.content.Context;

import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.persistance.strategies.DbFlowCarStrategyTransaction;
import com.brisoft.androidorms.persistance.strategies.GreenDaoCarStrategyTransaction;
import com.brisoft.androidorms.persistance.strategies.ORMLiteStrategyTransaction;
import com.brisoft.androidorms.persistance.strategies.RoomCarStrategy;
import com.brisoft.androidorms.persistance.strategies.SprinklesCarStrategyTransaction;
import com.brisoft.androidorms.persistance.strategies.StrategyTransaction;
import com.brisoft.androidorms.persistance.strategies.SQLiteHelperCarStrategyTransaction;
import com.brisoft.androidorms.persistance.strategies.SugarCarStrategyTransaction;

/**
 * Created by carlos on 24/07/17.
 */

public final class TransactionFactory {

    private TransactionFactory(){}

    public final static class CarStrategyFactory{

        private CarStrategyFactory(){}

        public static StrategyTransaction<Car> getSqlHelper(Context context){
            return new SQLiteHelperCarStrategyTransaction(context);
        }

        public static StrategyTransaction<Car> getSugar(){
            return new SugarCarStrategyTransaction();
        }

        public static StrategyTransaction<Car> getSprinkles(){
            return new SprinklesCarStrategyTransaction();
        }

        public static StrategyTransaction<Car> getRoom(){
            return new RoomCarStrategy();
        }

        public static StrategyTransaction<Car> getOrmLite(){
            return new ORMLiteStrategyTransaction();
        }

        public static StrategyTransaction<Car> getGreenDao(){
            return new GreenDaoCarStrategyTransaction();
        }

        public static StrategyTransaction<Car> getDbFlow(){
            return new DbFlowCarStrategyTransaction();
        }

    }

}
