package com.brisoft.androidorms.persistance.strategies;

import com.brisoft.androidorms.models.Car;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.ModelAdapter;

import java.util.List;

/**
 * Created by carlos on 25/07/17.
 */

public class DbFlowCarStrategyTransaction implements StrategyTransaction<Car>{

    private ModelAdapter<Car> modelAdapter;

    public DbFlowCarStrategyTransaction() {
        modelAdapter = FlowManager.getModelAdapter(Car.class);
    }

    @Override
    public long create(Car record) {
        return modelAdapter.insert(record);
    }

    @Override
    public void createAll(List<Car> records) {
        modelAdapter.insertAll(records);
    }

    @Override
    public Car read(long id) {
        return null;
    }

    @Override
    public List<Car> readAll() {
        return SQLite.select().from(Car.class).queryList();
    }

    @Override
    public boolean update(Car record) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public boolean deleteAll() {
        SQLite.delete(Car.class).execute();

        return SQLite.select().from(Car.class).count() == 0;
    }

    private ModelAdapter<Car> getModelAdapter(){
        return modelAdapter;
    }
}
