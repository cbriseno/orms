package com.brisoft.androidorms.persistance.strategies;

import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.persistance.dblibraries.room.AppDatabase;
import com.brisoft.androidorms.persistance.dblibraries.room.CarDao_Impl;

import java.util.List;

/**
 * Created by carlos on 25/07/17.
 */

public class RoomCarStrategy implements StrategyTransaction<Car> {

    private CarDao_Impl carDao;

    public RoomCarStrategy() {
        carDao= new CarDao_Impl(AppDatabase.getInstance());
    }

    @Override
    public long create(Car record) {
        return carDao.insert(record);
    }

    @Override
    public void createAll(List<Car> records) {
        carDao.insert(records);
    }

    @Override
    public Car read(long id) {
        return null;
    }

    @Override
    public List<Car> readAll() {
        return carDao.getAll();
    }

    @Override
    public boolean update(Car record) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public boolean deleteAll() {
        carDao.deleteAll();

        return carDao.count() == 0;
    }


}
