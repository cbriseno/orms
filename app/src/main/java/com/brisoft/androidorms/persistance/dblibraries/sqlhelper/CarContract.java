package com.brisoft.androidorms.persistance.dblibraries.sqlhelper;

import android.provider.BaseColumns;

/**
 * Created by carlos on 21/07/17.
 */

public final class CarContract implements BaseColumns{

    private CarContract(){}

    public static final String TABLE_NAME = "car";
    public static final String COLUMN_MODEL = "model";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_HEX_COLOR = "hexcolor";
    public static final String COLUMN_HP = "hp";

    public static final String SENTENCE_CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
            _ID             + " INTEGER PRIMARY KEY, " +
            COLUMN_MODEL    + " TEXT, " +
            COLUMN_YEAR     + " INTEGER, "+
                    COLUMN_HEX_COLOR + " TEXT, " +
            COLUMN_HP       + " INTEGER)";

    public static final String SENTENCE_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static final String WHERE_ID =
            "WHERE " + _ID + " = ?";

    public static final String SENTENCE_READ_BY_ID =
            "SELECT * FROM " + TABLE_NAME + " " + WHERE_ID;

    public static final String SENTENCE_READ_ALL =
            "SELECT * FROM " + TABLE_NAME;

}
