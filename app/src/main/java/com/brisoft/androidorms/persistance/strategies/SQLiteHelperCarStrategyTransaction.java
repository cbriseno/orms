package com.brisoft.androidorms.persistance.strategies;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.persistance.dblibraries.sqlhelper.CarContract;
import com.brisoft.androidorms.persistance.dblibraries.sqlhelper.DBHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlos on 21/07/17.
 */

public class SQLiteHelperCarStrategyTransaction implements StrategyTransaction<Car> {

    private Context context;

    public SQLiteHelperCarStrategyTransaction(Context context) {
        this.context = context;
    }

    @Override
    public long create(Car record) {
        long newRecordId;
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(CarContract.COLUMN_MODEL, record.getModel());
        contentValues.put(CarContract.COLUMN_HEX_COLOR, record.getHexColor());
        contentValues.put(CarContract.COLUMN_YEAR, record.getYear());
        contentValues.put(CarContract.COLUMN_HP, record.getHp());

        newRecordId = database.insert(CarContract.TABLE_NAME, null, contentValues);
        record.setRowId(newRecordId);
        database.close();

        return newRecordId;
    }

    @Override
    public void createAll(List<Car> records) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.beginTransaction();

        for (Car record: records){
            ContentValues contentValues = new ContentValues();

            contentValues.put(CarContract.COLUMN_MODEL, record.getModel());
            contentValues.put(CarContract.COLUMN_HEX_COLOR, record.getHexColor());
            contentValues.put(CarContract.COLUMN_YEAR, record.getYear());
            contentValues.put(CarContract.COLUMN_HP, record.getHp());

            database.insert(CarContract.TABLE_NAME, null, contentValues);
        }

        database.setTransactionSuccessful();
        database.endTransaction();
        database.close();
    }

    @Override
    public Car read(long id) {
        Car car = null;
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor cursor = database.rawQuery(CarContract.SENTENCE_READ_BY_ID, new String[]{String.valueOf(id)});

        if (cursor.moveToFirst()){
            car = new Car();

            car.setHexColor(cursor.getString(cursor.getColumnIndex(CarContract.COLUMN_HEX_COLOR)));
            car.setYear(cursor.getInt(cursor.getColumnIndex(CarContract.COLUMN_YEAR)));
            car.setModel(cursor.getString(cursor.getColumnIndex(CarContract.COLUMN_MODEL)));
            car.setHp(cursor.getInt(cursor.getColumnIndex(CarContract.COLUMN_HP)));
        }

        database.close();

        return car;
    }

    @Override
    public List<Car> readAll() {
        List<Car> carList = new ArrayList<>();
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor cursor = database.rawQuery(CarContract.SENTENCE_READ_ALL, null);

        while (cursor.moveToNext()) {
            Car car = new Car();

            car.setHexColor(cursor.getString(cursor.getColumnIndex(CarContract.COLUMN_HEX_COLOR)));
            car.setYear(cursor.getInt(cursor.getColumnIndex(CarContract.COLUMN_YEAR)));
            car.setModel(cursor.getString(cursor.getColumnIndex(CarContract.COLUMN_MODEL)));
            car.setHp(cursor.getInt(cursor.getColumnIndex(CarContract.COLUMN_HP)));

            carList.add(car);
        }

        database.close();

        return carList;
    }


    public boolean update(Car record) {Car car = null;
        int affectedRows = 0;
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getReadableDatabase();

        if (read(record.getRowId()) != null){
            ContentValues contentValues = new ContentValues();

            contentValues.put(CarContract.COLUMN_MODEL, record.getModel());
            contentValues.put(CarContract.COLUMN_HEX_COLOR, record.getHexColor());
            contentValues.put(CarContract.COLUMN_YEAR, record.getYear());
            contentValues.put(CarContract.COLUMN_HP, record.getHp());

            affectedRows = database.update(CarContract.TABLE_NAME, contentValues, CarContract.WHERE_ID, new String[]{String.valueOf(car.getRowId())});

            database.close();
        }

        return affectedRows > 0;
    }

//    @Override
    public void updateAll(List<Car> records) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.beginTransaction();

        for (Car record: records){
            ContentValues contentValues = new ContentValues();

            contentValues.put(CarContract.COLUMN_MODEL, record.getModel());
            contentValues.put(CarContract.COLUMN_HEX_COLOR, record.getHexColor());
            contentValues.put(CarContract.COLUMN_YEAR, record.getYear());
            contentValues.put(CarContract.COLUMN_HP, record.getHp());

            database.insert(CarContract.TABLE_NAME, null, contentValues);
            database.setTransactionSuccessful();
            database.endTransaction();
        }

        database.close();
    }

    @Override
    public boolean delete(long id) {
        int affectedRows = 0;
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getReadableDatabase();

        affectedRows = database.delete(CarContract.TABLE_NAME, CarContract.WHERE_ID, new String[]{String.valueOf(id)});

        return affectedRows > 0;
    }

    @Override
    public boolean deleteAll() {
        long count = readAll().size();
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getReadableDatabase();

        return count == database.delete(CarContract.TABLE_NAME, "_id != 0", null);
    }
}
