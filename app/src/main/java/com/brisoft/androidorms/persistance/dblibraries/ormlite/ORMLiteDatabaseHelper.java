package com.brisoft.androidorms.persistance.dblibraries.ormlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.brisoft.androidorms.models.Car;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.File;
import java.io.InputStream;
import java.sql.SQLException;

/**
 * Created by carlos on 25/07/17.
 */

public class ORMLiteDatabaseHelper extends OrmLiteSqliteOpenHelper{

    private static final String DATABASE_NAME = "ormlite.db";
    private static final int DATABASE_VERSION = 1;
    private static ORMLiteDatabaseHelper databaseHelper;

    private Dao<Car, Integer> carDao = null;

    public static void initialize(Context context){
        databaseHelper = new ORMLiteDatabaseHelper(context);
    }

    public static ORMLiteDatabaseHelper getInstance(){
        return databaseHelper;
    }

    public ORMLiteDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Car.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Car.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<Car, Integer> getCarDao() throws SQLException {
        if (carDao == null){
            carDao = getDao(Car.class);
        }

        return carDao;
    }

    public void close(){
        carDao = null;
    }
}
