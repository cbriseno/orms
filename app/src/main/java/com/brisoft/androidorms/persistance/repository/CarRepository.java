package com.brisoft.androidorms.persistance.repository;

import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.persistance.strategies.StrategyTransaction;

import java.util.List;

/**
 * Created by carlos on 21/07/17.
 */

public class CarRepository{

    private StrategyTransaction<Car> strategyTransaction;

    public CarRepository(StrategyTransaction<Car> strategyTransaction) {
        this.strategyTransaction = strategyTransaction;
    }

    public void setStrategyTransaction(StrategyTransaction<Car> strategyTransaction) {
        this.strategyTransaction = strategyTransaction;
    }

    public StrategyTransaction<Car> getStrategyTransaction() {
        return strategyTransaction;
    }

    public long create(Car car){
        return strategyTransaction.create(car);
    }

    public void createAll(List<Car> carList){
        strategyTransaction.createAll(carList);
    }

    public Car read(long id){
        return strategyTransaction.read(id);
    }

    public List<Car> readAll(){
        return strategyTransaction.readAll();
    }

    public boolean deleteAll(){
        return strategyTransaction.deleteAll();
    }
}
