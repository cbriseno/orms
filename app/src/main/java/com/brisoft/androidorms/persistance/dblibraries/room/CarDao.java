package com.brisoft.androidorms.persistance.dblibraries.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.brisoft.androidorms.models.Car;

import java.util.List;

/**
 * Created by carlos on 25/07/17.
 */
@Dao
public interface CarDao {

    @Query("SELECT * FROM car")
    List<Car> getAll();

    @Insert
    long insert(Car car);

    @Insert
    void insert(List<Car> carList);

    @Delete
    void delete(Car car);

    @Query("DELETE FROM car")
    void deleteAll();

    @Query("SELECT COUNT(rowid) FROM car")
    long count();

}
