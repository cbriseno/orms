package com.brisoft.androidorms.persistance.dblibraries.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.brisoft.androidorms.models.Car;

/**
 * Created by carlos on 25/07/17.
 */
@Database(entities = {Car.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase{

    private static AppDatabase appDatabase;

    public abstract CarDao carDao();

    public static void initialize(Context context){
        appDatabase = Room.databaseBuilder(context, AppDatabase.class, "myroomdb").build();
    }

    public static AppDatabase getInstance(){
        return appDatabase;
    }

}
