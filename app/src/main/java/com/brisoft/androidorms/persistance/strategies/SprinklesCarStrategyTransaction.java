package com.brisoft.androidorms.persistance.strategies;

import com.brisoft.androidorms.models.Car;

import java.util.List;

import se.emilsjolander.sprinkles.CursorList;
import se.emilsjolander.sprinkles.ModelList;
import se.emilsjolander.sprinkles.Query;

/**
 * Created by carlos on 25/07/17.
 */

public class SprinklesCarStrategyTransaction implements StrategyTransaction<Car> {

    @Override
    public long create(Car record) {
        if (record.getRowId() == null){
            record.setRowId(0L);
        }
        
        record.save();

        return record.getRowId();
    }

    @Override
    public void createAll(List<Car> records) {
        ModelList<Car> modelList = new ModelList<>(records);

        modelList.saveAll();
    }

    @Override
    public Car read(long id) {
        return null;
    }

    @Override
    public List<Car> readAll() {
        CursorList<Car> carCursorList = Query.all(Car.class).get();
        List<Car> carList = carCursorList.asList();

        carCursorList.close();

        return carList;
    }

    @Override
    public boolean update(Car record) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public boolean deleteAll() {
        CursorList<Car> carCursorList = Query.all(Car.class).get();
        ModelList<Car> carModelList = ModelList.from(carCursorList);
        int remainingItems;

        carModelList.deleteAll();
        carCursorList.close();
        carCursorList = Query.all(Car.class).get();
        remainingItems = carCursorList.size();
        carCursorList.close();

        return remainingItems == 0;
    }
}
