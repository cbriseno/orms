package com.brisoft.androidorms.persistance.strategies;

import java.util.List;

/**
 * Created by carlos on 21/07/17.
 */

public interface StrategyTransaction<M> {

    enum Action{
        InsertFast,
        InsertSlow,
        ReadById,
        ReadAll,
        UpdateById,
        UpdateAll,
        DeleteById,
        DeleteAll
    }

    long create(M record);
    void createAll(List<M> records);
    M read(long id);
    List<M> readAll();
    boolean update(M record);
//    void updateAll(List<M> records);
    boolean delete(long id);
    boolean deleteAll();

}
