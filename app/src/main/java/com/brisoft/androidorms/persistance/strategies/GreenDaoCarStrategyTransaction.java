package com.brisoft.androidorms.persistance.strategies;

import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.models.CarDao;
import com.brisoft.androidorms.models.DaoSession;
import com.brisoft.androidorms.persistance.dblibraries.greendao.DatabaseProvider;

import java.util.List;

/**
 * Created by carlos on 25/07/17.
 */

public class GreenDaoCarStrategyTransaction implements StrategyTransaction<Car> {
    @Override
    public long create(Car record) {
        return getDao().insert(record);
    }

    @Override
    public void createAll(List<Car> records) {
        getDao().insertInTx(records);
    }

    @Override
    public Car read(long id) {
        return null;
    }

    @Override
    public List<Car> readAll() {
        return getDao().loadAll();
    }

    @Override
    public boolean update(Car record) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public boolean deleteAll() {
        getDao().deleteAll();

        return getDao().count() == 0;
    }

    private CarDao getDao(){
        return DatabaseProvider.getDaoSession().getCarDao();
    }
}
