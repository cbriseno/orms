package com.brisoft.androidorms.persistance.strategies;

import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.persistance.dblibraries.ormlite.ORMLiteDatabaseHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlos on 25/07/17.
 */

public class ORMLiteStrategyTransaction implements StrategyTransaction<Car> {
    @Override
    public long create(Car record) {
        long recordId = 0;

        try {
            getDao().create(record);
            recordId = record.getRowId();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return recordId;
    }

    @Override
    public void createAll(List<Car> records) {
        try {
            getDao().create(records);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Car read(long id) {
        return null;
    }

    @Override
    public List<Car> readAll() {
        List<Car> carList = new ArrayList<>();

        try {
            carList = getDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return carList;
    }

    @Override
    public boolean update(Car record) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public boolean deleteAll() {
        long rowCount = -1;
        long affectedRows = 0;

        try {
            DeleteBuilder<Car, Integer> deleteBuilder = getDao().deleteBuilder();

            deleteBuilder.where().isNotNull("id");

            rowCount = getDao().countOf();
            affectedRows = deleteBuilder.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rowCount == affectedRows;
    }

    private Dao<Car, Integer> getDao() throws SQLException {
        return ORMLiteDatabaseHelper.getInstance().getCarDao();
    }
}