package com.brisoft.androidorms.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.brisoft.androidorms.R;
import com.brisoft.androidorms.fragments.ActionExecutionFragment;
import com.brisoft.androidorms.fragments.ActionSelectionFragment;
import com.brisoft.androidorms.fragments.ToolSelectionFragment;
import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.persistance.factory.TransactionFactory;
import com.brisoft.androidorms.persistance.repository.CarRepository;
import com.brisoft.androidorms.persistance.strategies.StrategyTransaction;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements ToolSelectionFragment.OnToolSelectedListener, ActionSelectionFragment.OnActionSelectedListener {

    private static final String TAG = "MainActivity";

    @BindView(R.id.tvSelectedTool)
    TextView tvSelectedTool;

    @BindView(R.id.tvSelectedAction)
    TextView tvSelectedAction;

    @BindView(R.id.vToolSelectionFragmentContainer)
    View vToolSelectionFragmentContainer;

    @BindView(R.id.vActionSelectionFragmentContainer)
    View vActionSelectionFragmentContainer;

    @BindView(R.id.vActionExecutionFragmentContainer)
    View vActionExecutionFragmentContainer;

    private ActionExecutionFragment actionExecutionFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ToolSelectionFragment toolSelectionFragment = (ToolSelectionFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_tool_selection));
        ActionSelectionFragment actionSelectionFragment = (ActionSelectionFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_action_selection));
        actionExecutionFragment = (ActionExecutionFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_action_execution));

        toolSelectionFragment.setOnToolSelectedListener(this);
        actionSelectionFragment.setOnActionSelectedListener(this);

        vToolSelectionFragmentContainer.setVisibility(View.GONE);
        vActionSelectionFragmentContainer.setVisibility(View.GONE);
        vActionExecutionFragmentContainer.setVisibility(View.GONE);

        tvSelectedTool.setText(getString(R.string.main_selected_tool, getString(R.string.general_none)));
        tvSelectedAction.setText(getString(R.string.main_selected_action, getString(R.string.general_none)));
    }

    @OnClick(R.id.tvSelectedTool)
    public void onClickSelectedTool(){
        if (vToolSelectionFragmentContainer.getVisibility() == View.VISIBLE) {
            vToolSelectionFragmentContainer.setVisibility(View.GONE);
        }else{
            vToolSelectionFragmentContainer.setVisibility(View.VISIBLE);

            if (vActionExecutionFragmentContainer.getVisibility() != View.VISIBLE && actionExecutionFragment.getAction() != null) {
                vActionExecutionFragmentContainer.setVisibility(View.VISIBLE);
            }
        }
    }

    @OnClick(R.id.tvSelectedAction)
    public void onClickSelectedAction(){
        if (vActionSelectionFragmentContainer.getVisibility() == View.VISIBLE) {
            vActionSelectionFragmentContainer.setVisibility(View.GONE);
        }else{
            vActionSelectionFragmentContainer.setVisibility(View.VISIBLE);

            if (vActionExecutionFragmentContainer.getVisibility() != View.VISIBLE && actionExecutionFragment.getStrategyTransaction() != null) {
                vActionExecutionFragmentContainer.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onToolSelected(int toolNameResourceId, StrategyTransaction<Car> selectedStrategyTransaction) {
        tvSelectedTool.setText(getString(R.string.main_selected_tool, getString(toolNameResourceId)));
        vToolSelectionFragmentContainer.setVisibility(View.GONE);
        actionExecutionFragment.setStrategyTransaction(selectedStrategyTransaction);
    }

    @Override
    public void onActonSelected(int actionNameResourceId, StrategyTransaction.Action selectedAction) {
        tvSelectedAction.setText(getString(R.string.main_selected_action, getString(actionNameResourceId)));
        vActionSelectionFragmentContainer.setVisibility(View.GONE);
        actionExecutionFragment.setAction(selectedAction);
    }
}
