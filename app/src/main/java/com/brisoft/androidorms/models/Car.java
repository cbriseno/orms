package com.brisoft.androidorms.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.brisoft.androidorms.persistance.dblibraries.dbflow.DbFlowDatabase;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.orm.dsl.Table;

import org.greenrobot.greendao.annotation.Id;

import se.emilsjolander.sprinkles.Model;
import se.emilsjolander.sprinkles.annotations.AutoIncrement;
import se.emilsjolander.sprinkles.annotations.Column;
import se.emilsjolander.sprinkles.annotations.Key;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Keep;

/**
 * Created by carlos on 21/07/17.
 */
@Table
@se.emilsjolander.sprinkles.annotations.Table("Car")
@Entity//ROOM
@DatabaseTable(tableName = "Car")
@org.greenrobot.greendao.annotation.Entity
@com.raizlabs.android.dbflow.annotation.Table(database = DbFlowDatabase.class)
public class Car extends Model{

    @Key
    @AutoIncrement
    @Column("id")
    @PrimaryKey(autoGenerate = true)//ROOM
    @DatabaseField(generatedId = true)
    @Id(autoincrement = true)
    @com.raizlabs.android.dbflow.annotation.PrimaryKey
    private Long rowId;
    @Column("model")
    @ColumnInfo(name = "model")// ROOM
    @DatabaseField
    @com.raizlabs.android.dbflow.annotation.Column
    private String model;
    @Column("year")
    @ColumnInfo(name = "year")
    @DatabaseField
    @com.raizlabs.android.dbflow.annotation.Column
    private int year;
    @Column("hexcolor")
    @ColumnInfo(name = "hexcolor")
    @DatabaseField
    @com.raizlabs.android.dbflow.annotation.Column
    private String hexColor;
    @Column("hp")
    @ColumnInfo(name = "hp")
    @DatabaseField
    @com.raizlabs.android.dbflow.annotation.Column
    private int hp;
    
    @Keep
    public Car() {
    }

    @Keep
    @Ignore
    public Car(Long rowId, String model, int year, String hexColor, int hp) {
        this.rowId = rowId;
        this.model = model;
        this.year = year;
        this.hexColor = hexColor;
        this.hp = hp;
    }

    public Long getRowId() {
        return rowId;
    }

    public void setRowId(Long rowId) {
        this.rowId = rowId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getHexColor() {
        return hexColor;
    }

    public void setHexColor(String hexColor) {
        this.hexColor = hexColor;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

}
