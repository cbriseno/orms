package com.brisoft.androidorms.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.brisoft.androidorms.R;
import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.persistance.factory.TransactionFactory;
import com.brisoft.androidorms.persistance.strategies.StrategyTransaction;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by carlos on 17/08/17.
 */

public class ToolSelectionFragment extends Fragment{

    private OnToolSelectedListener onToolSelectedListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vLayout = inflater.inflate(R.layout.fragment_tool_selection, container, false);

        ButterKnife.bind(this, vLayout);

        return vLayout;
    }

    @OnClick({R.id.btSugar, R.id.btDbFlow, R.id.btGreenDao, R.id.btOrmLite, R.id.btRoom, R.id.btSliteHelper, R.id.btSprinkles})
    public void click(Button button){
        int toolNameResourceId = 0;
        StrategyTransaction<Car> strategyTransaction = null;

        switch (button.getId()){
            case R.id.btSliteHelper:
                toolNameResourceId = R.string.sqlite_helper_title;
                strategyTransaction = TransactionFactory.CarStrategyFactory.getSqlHelper(getContext());
                break;
            case R.id.btSugar:
                toolNameResourceId = R.string.sugar_title;
                strategyTransaction = TransactionFactory.CarStrategyFactory.getSugar();
                break;
            case R.id.btSprinkles:
                toolNameResourceId = R.string.sprinkless_title;
                strategyTransaction = TransactionFactory.CarStrategyFactory.getSprinkles();
                break;
            case R.id.btRoom:
                toolNameResourceId = R.string.room_title;
                strategyTransaction = TransactionFactory.CarStrategyFactory.getRoom();
                break;
            case R.id.btOrmLite:
                toolNameResourceId = R.string.ormlite_title;
                strategyTransaction = TransactionFactory.CarStrategyFactory.getOrmLite();
                break;
            case R.id.btGreenDao:
                toolNameResourceId = R.string.green_dao_title;
                strategyTransaction = TransactionFactory.CarStrategyFactory.getGreenDao();
                break;
            case R.id.btDbFlow:
                toolNameResourceId = R.string.dbflow_title;
                strategyTransaction = TransactionFactory.CarStrategyFactory.getDbFlow();
                break;
        }

        if (onToolSelectedListener != null) {
            onToolSelectedListener.onToolSelected(toolNameResourceId, strategyTransaction);
        }
    }

    public void setOnToolSelectedListener(OnToolSelectedListener onToolSelectedListener) {
        this.onToolSelectedListener = onToolSelectedListener;
    }

    public interface OnToolSelectedListener{
        void onToolSelected(int toolNameResourceId, StrategyTransaction<Car> selectedStrategyTransaction);
    }

}
