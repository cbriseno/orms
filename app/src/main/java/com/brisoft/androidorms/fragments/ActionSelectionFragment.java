package com.brisoft.androidorms.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.brisoft.androidorms.R;
import com.brisoft.androidorms.persistance.strategies.StrategyTransaction;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by carlos on 17/08/17.
 */

public class ActionSelectionFragment extends Fragment{

    private OnActionSelectedListener onActionSelectedListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vLayout = inflater.inflate(R.layout.fragment_action_selection, container, false);

        ButterKnife.bind(this, vLayout);

        return vLayout;
    }

    @OnClick({
            R.id.btInsertSlow, R.id.btInsertFast,
            R.id.btReadById, R.id.btReadAll,
            R.id.btUpdateById, R.id.btUpdateAll,
            R.id.btDeleteById, R.id.btDeleteAll})
    public void onClick(Button button){
        int actionNameResourceId = 0;
        StrategyTransaction.Action selectedAction = null;

        switch (button.getId()){
            case R.id.btInsertSlow:
                actionNameResourceId = R.string.action_insert_slow;
                selectedAction = StrategyTransaction.Action.InsertSlow;
                break;
            case R.id.btInsertFast:
                actionNameResourceId = R.string.action_insert_fast;
                selectedAction = StrategyTransaction.Action.InsertFast;
                break;
            case R.id.btReadById:
                actionNameResourceId = R.string.action_read_by_id;
                selectedAction = StrategyTransaction.Action.ReadById;
                break;
            case R.id.btReadAll:
                actionNameResourceId = R.string.action_read_all;
                selectedAction = StrategyTransaction.Action.ReadAll;
                break;
            case R.id.btUpdateById:
                actionNameResourceId = R.string.action_update_by_id;
                selectedAction = StrategyTransaction.Action.UpdateById;
                break;
            case R.id.btUpdateAll:
                actionNameResourceId = R.string.action_update_all;
                selectedAction = StrategyTransaction.Action.UpdateAll;
                break;
            case R.id.btDeleteById:
                actionNameResourceId = R.string.action_delete_by_id;
                selectedAction = StrategyTransaction.Action.DeleteById;
                break;
            case R.id.btDeleteAll:
                actionNameResourceId = R.string.action_delete_all;
                selectedAction = StrategyTransaction.Action.DeleteAll;
                break;
        }

        if (onActionSelectedListener != null) {
            onActionSelectedListener.onActonSelected(actionNameResourceId, selectedAction);
        }
    }

    public void setOnActionSelectedListener(OnActionSelectedListener onActionSelectedListener) {
        this.onActionSelectedListener = onActionSelectedListener;
    }

    public interface OnActionSelectedListener{
        void onActonSelected(int actionNameResourceId, StrategyTransaction.Action selectedAction);
    }
}
