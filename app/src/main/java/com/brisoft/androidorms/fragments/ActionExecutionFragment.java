package com.brisoft.androidorms.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.brisoft.androidorms.R;
import com.brisoft.androidorms.concurrent.BaseAsyncTask;
import com.brisoft.androidorms.concurrent.CrateRecordsAsyncTask;
import com.brisoft.androidorms.concurrent.DeleteAllAsyncTask;
import com.brisoft.androidorms.concurrent.ReadAllRecordsAsyncTask;
import com.brisoft.androidorms.models.Car;
import com.brisoft.androidorms.persistance.factory.TransactionFactory;
import com.brisoft.androidorms.persistance.repository.CarRepository;
import com.brisoft.androidorms.persistance.strategies.StrategyTransaction;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by carlos on 18/08/17.
 */

public class ActionExecutionFragment extends Fragment implements BaseAsyncTask.OnTaskCompletedListener {

    private static final String TAG ="ActionExecutionFragment";

    @BindView(R.id.etNumberPicker)
    EditText etNumberPicker;

    @BindView(R.id.tilNumberPicker)
    TextInputLayout tilNumberPicker;

    @BindView(R.id.btRun)
    Button btRun;

    @BindView(R.id.pbLoader)
    ProgressBar pbLoader;

    @BindView(R.id.tvProgress)
    TextView tvProgress;

    private CarRepository carRepository;
    private StrategyTransaction.Action action;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        carRepository = new CarRepository(TransactionFactory.CarStrategyFactory.getSqlHelper(getContext()));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vLayout = inflater.inflate(R.layout.fragment_action_execution, container, false);

        ButterKnife.bind(this, vLayout);

        etNumberPicker.setVisibility(View.GONE);
        btRun.setVisibility(View.GONE);
        pbLoader.setVisibility(View.INVISIBLE);
        tvProgress.setVisibility(View.INVISIBLE);

        return vLayout;
    }

    @OnClick(R.id.btRun)
    public void clickRun(){
        hideKeyboard();
        showLoader();

        switch (action){
            case InsertFast:
            case InsertSlow:
                createRecords(action);
                break;
            case ReadById:
                break;
            case ReadAll:
                readAllRecords();
                break;
            case UpdateById:
                break;
            case UpdateAll:
                break;
            case DeleteById:
                break;
            case DeleteAll:
                deleteAllRecords();
                break;
        }
    }

    public StrategyTransaction<Car> getStrategyTransaction(){
        return carRepository.getStrategyTransaction();
    }

    public StrategyTransaction.Action getAction() {
        return action;
    }

    public void setStrategyTransaction(StrategyTransaction<Car> strategyTransaction){
        carRepository.setStrategyTransaction(strategyTransaction);
    }

    public void setAction(StrategyTransaction.Action action) {
        boolean showEditText = false;

        this.action = action;

        if (btRun.getVisibility() != View.VISIBLE){
            btRun.setVisibility(View.VISIBLE);
        }

        switch (action){
            case InsertFast:
            case InsertSlow:
                showEditText = true;
                tilNumberPicker.setHint(getString(R.string.action_execution_records_hint));
                break;
            case ReadById:
            case UpdateById:
            case DeleteById:
                showEditText = true;
                tilNumberPicker.setHint(getString(R.string.action_execution_id_hint));
                break;
            case ReadAll:
            case UpdateAll:
            case DeleteAll:
                showEditText = false;
                break;
        }

        etNumberPicker.setVisibility(showEditText ? View.VISIBLE : View.GONE);
    }

    private void createRecords(StrategyTransaction.Action action){
        int recordsToCreate = 0;

        try {
            recordsToCreate = Integer.valueOf(etNumberPicker.getText().toString());

            new CrateRecordsAsyncTask(carRepository, recordsToCreate, action == StrategyTransaction.Action.InsertFast)
                    .setOnTaskCompletedListener(this)
                    .execute();
        }catch (NumberFormatException e){
            e.printStackTrace();
            Toast.makeText(getContext(), R.string.action_execution_error_invalid_number, Toast.LENGTH_SHORT).show();
        }
    }

    private void readAllRecords(){
        new ReadAllRecordsAsyncTask(carRepository)
                .setOnTaskCompletedListener(this)
                .execute();
    }

    private void deleteAllRecords(){
        new DeleteAllAsyncTask(carRepository)
                .setOnTaskCompletedListener(this)
                .execute();
    }

    private void showLoader(){
        pbLoader.setVisibility(View.VISIBLE);
    }

    private void hideLoader(){
        pbLoader.setVisibility(View.INVISIBLE);
    }

    private void hideKeyboard() {
        Activity activity = getActivity();

        if (activity != null && activity.getCurrentFocus() != null){
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onTaskCompleted(long millisRequired) {
        hideLoader();

        if (tvProgress.getVisibility() != View.VISIBLE){
            tvProgress.setVisibility(View.VISIBLE);
        }

        tvProgress.setText(getString(R.string.action_execution_task_progress, millisRequired/1000f));
    }
}
